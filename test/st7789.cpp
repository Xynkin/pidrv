#include <st7789.h>

void sig_handler(int signum) {
    if( signum == SIGINT ){
        puts("CTRL+C received");
        std::exit(0);
    }
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {

    signal(SIGINT, sig_handler);

    auto bus =
        SPI{ "/dev/spidev0.1" }
        .mode(SPI_MODE_0)
        .lsb_first(false)
        .bits_per_word(8)
        .speed(90_MHz);

    auto pins =
        GPIO{ "gpiochip0" }
        .outputs({{9, false}, {19, true}});

    using Display = ST7789<240u, 240u, 40u, 0u>;

    Display display {bus, pins[9], Display::request_refresh_rate(60.f).value_or(Display::refresh_controls_t{})};

    Display::RGB565::Packed color {0x781c};
    for( ;; ){
        display(color);
    }

    return 0;
}
