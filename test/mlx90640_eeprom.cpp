#include <cstdio>

#include <mlx90640.h>

int main(int argc, char *argv[]) {

    if( argc != 1 ){
        if( MLX90640::dump_eeprom(argv[1]) ){
            puts("Success");
        }else{
            puts("Failed");
        }
    }else{
        printf("USAGE: ./%s file_name\n", argv[0]);
    }

    return 0;
}
