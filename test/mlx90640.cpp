#include <cstdio>
#include <chrono>
#include <thread>

#include <mlx90640.h>

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) {

    using namespace std::chrono_literals;

    MLX90640 mlx90640 {MLX90640::refresh_rate_t::_04_HZ};
    float frame[MLX90640::WIDTH*MLX90640::HEIGHT];

    for( ;; ){
        const auto start = std::chrono::steady_clock::now();
        while( !mlx90640((MLX90640::ToPtr)frame) ){
            std::this_thread::sleep_for(5ms);
        }
        const auto finish = std::chrono::steady_clock::now();

        printf(
            "Got a frame in %llu ms\n",
            std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
        );
    }

    return 0;
}
