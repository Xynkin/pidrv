#pragma once

constexpr unsigned long long operator"" _bit(unsigned long long x) {
  return 1<<x;
}
constexpr unsigned bit(unsigned N) {
  return 1<<N;
}
constexpr unsigned bit(unsigned N, unsigned X) {
  return X<<N;
}
constexpr unsigned bits(unsigned MSB, unsigned LSB, unsigned X) {
  return (X&((1<<(MSB - LSB + 1)) - 1))<<LSB;
}
