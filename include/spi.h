#pragma once

#include <stdexcept>
#include <system_error>

#include <fcntl.h>
#include <getopt.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <units.h>

class SPI {
public:
    explicit SPI(const char* device)
        : mFD { open(device, O_RDWR) }
        , mTransfer {
            .tx_buf = 0,
            .rx_buf = 0,
            .len = 0,
            .speed_hz = 1_MHz,
            .delay_usecs = 0,
            .bits_per_word = 8,
            .cs_change = 0,
            .tx_nbits = 0,
            .rx_nbits = 0,
            .word_delay_usecs = 0,
            .pad = 0
        }
    {
        if( mFD < 0 ){
            throw std::system_error(
                std::error_code(errno, std::system_category()), device);
        }
    }
    SPI(const SPI&) = delete;
    SPI(SPI&& rhs)
        : mFD{ -1 }
        , mTransfer {rhs.mTransfer}
    {
        std::swap(mFD, rhs.mFD);
    }
    SPI& operator=(SPI) = delete;
    SPI& operator=(const SPI&) = delete;
    SPI& operator=(SPI&& rhs)
    {
        std::swap(mFD, rhs.mFD);
        mTransfer = rhs.mTransfer;
        return *this;
    }
    ~SPI() {
        printf("~SPI() %d\n", mFD);
        if( mFD >= 0 ){
            close(mFD);
        }
    }

    SPI& mode(uint8_t mode) & {

        if( ioctl(mFD, SPI_IOC_WR_MODE, &mode) < 0 ){
            throw std::system_error(
                std::error_code(errno, std::system_category()),
                "Can't set SPI mode");
        }

        return *this;
    }
    SPI&& mode(uint8_t mode) && {
        return std::move(static_cast<SPI*>(this)->mode(mode));
    }

    SPI& lsb_first(uint8_t lsb_first) & {

        if( ioctl(mFD, SPI_IOC_WR_LSB_FIRST, &lsb_first) < 0 ){
            throw std::system_error(
                std::error_code(errno, std::system_category()),
                "Can't set LSB first mode");
        }

        return *this;
    }
    SPI&& lsb_first(uint8_t lsb_first) && {
        return std::move(static_cast<SPI*>(this)->lsb_first(lsb_first));
    }

    SPI& bits_per_word(uint8_t bits_per_word) & {
        mTransfer.bits_per_word = bits_per_word;

        if( ioctl(mFD, SPI_IOC_WR_BITS_PER_WORD, &bits_per_word) < 0 ){
            throw std::system_error(
                std::error_code(errno, std::system_category()),
                "Can't set bits per word");
        }

        return *this;
    }
    SPI&& bits_per_word(uint8_t bits_per_word) && {
        return std::move(static_cast<SPI*>(this)->bits_per_word(bits_per_word));
    }

    SPI& speed(unsigned speed_hz) & {
        mTransfer.speed_hz = speed_hz;

        if( ioctl(mFD, SPI_IOC_WR_MAX_SPEED_HZ, &speed_hz) < 0 ){
            throw std::system_error(
                std::error_code(errno, std::system_category()),
                "Can't set max speed hz");
        }

        return *this;
    }
    SPI&& speed(unsigned speed_hz) && {
        return std::move(static_cast<SPI*>(this)->speed(speed_hz));
    }

    // uint8_t xfer(uint8_t val) {
    //     uint8_t rx_buf;
    //     mTransfer.tx_buf = __u64(&val);
    //     mTransfer.rx_buf = __u64(&rx_buf);
    //     mTransfer.len = 1u;
    //     if( ioctl(mFD, SPI_IOC_MESSAGE(1), &mTransfer) < 1 ){
    //         throw std::system_error(
    //             std::error_code(errno, std::system_category()),
    //             "Can't send SPI message");
    //     }
    //     return rx_buf;
    // }

    void send(const uint8_t* data, size_t size) const {
        while( size ){
            const auto chunk = std::min(size, size_t(4096u));
            if( write(mFD, data, chunk) < ssize_t(chunk) ){
                throw std::system_error(
                    std::error_code(errno, std::system_category()),
                    "Can't send data over SPI");
            }
            data += chunk;
            size -= chunk;
        }
    }
    void send(std::initializer_list<uint8_t> data) const {
        const auto size = data.size()*sizeof(decltype(data)::value_type);
        send(data.begin(), size);
    }

private:
    int mFD;
    spi_ioc_transfer mTransfer;
};
