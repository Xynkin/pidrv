#pragma once

#include <map>
#include <stdexcept>
#include <system_error>

#include <gpiod.h>

class GPIO {
public:
    class Pin {
    public:
        inline operator bool() const noexcept {
            return gpiod_line_get_value(mLine);
        }
        inline void operator()(bool state) const noexcept {
            gpiod_line_set_value(mLine, state);
        }

    private:
        friend GPIO;
        explicit Pin(gpiod_line* line) noexcept
            : mLine{ line }
        {}
    private:
        gpiod_line* mLine;
    };
public:
    explicit GPIO(const char* chipname)
        : mChip { gpiod_chip_open_by_name(chipname) }
    {
        if( !mChip ){
            throw std::system_error(
                std::error_code(errno, std::system_category()), chipname);
        }
    }
    GPIO(const GPIO&) = delete;
    GPIO(GPIO&& rhs)
        : mChip{ nullptr }
        , mLines {}
    {
        std::swap(mChip, rhs.mChip);
        std::swap(mLines, rhs.mLines);
    }
    GPIO& operator=(GPIO) = delete;
    GPIO& operator=(const GPIO&) = delete;
    GPIO& operator=(GPIO&& rhs)
    {
        std::swap(mChip, rhs.mChip);
        std::swap(mLines, rhs.mLines);
        return *this;
    }
    ~GPIO() {
        printf("~GPIO() %u\n", static_cast<unsigned>(mLines.size()));
        for( auto& line: mLines ){
            gpiod_line_release(line.second);
        }
        if( mChip ){
            gpiod_chip_close(mChip);
        }
    }

    GPIO& inputs(std::initializer_list<unsigned> list) & {
        for( const auto line: list ){
            if( mLines.count(line) ){
                throw std::runtime_error(
                    "GPIO already configured: " + std::to_string(line));
            }

            const auto p = gpiod_chip_get_line(mChip, line);
            if( !p ){
                throw std::system_error(
                    std::error_code(errno, std::system_category()),
                    "failed get GPIO: " + std::to_string(line));
            }
            if( gpiod_line_request_input(p, "GPIO") == -1 ){
                throw std::system_error(
                    std::error_code(errno, std::system_category()),
                    "failed to set GPIO as input: " + std::to_string(line));
            }
            mLines.emplace(line, p);
        }

        return *this;
    }
    GPIO&& inputs(std::initializer_list<unsigned> list) && {
        return std::move(static_cast<GPIO*>(this)->inputs(list));
    }

    GPIO& outputs(std::initializer_list<std::pair<unsigned, bool>> list) & {
        for( const auto& line: list ){
            if( mLines.count(line.first) ){
                throw std::runtime_error(
                    "GPIO already configured: " + std::to_string(line.first));
            }

            const auto p = gpiod_chip_get_line(mChip, line.first);
            if( !p ){
                throw std::system_error(
                    std::error_code(errno, std::system_category()),
                    "failed get GPIO: " + std::to_string(line.first));
            }
            if( gpiod_line_request_output(p, "GPIO", line.second) == -1 ){
                throw std::system_error(
                    std::error_code(errno, std::system_category()),
                    "failed to set GPIO as output: " + std::to_string(line.first));
            }
            mLines.emplace(line.first, p);
        }

        return *this;
    }
    GPIO&& outputs(std::initializer_list<std::pair<unsigned, bool>> list) && {
        return std::move(static_cast<GPIO*>(this)->outputs(list));
    }

    Pin operator[](unsigned pin) const noexcept {
        return Pin{mLines.at(pin)};
    }

private:
    gpiod_chip* mChip;
    std::map<unsigned, gpiod_line*> mLines;
};
