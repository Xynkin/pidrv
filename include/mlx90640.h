#pragma once

#include <cmath>
#include <cstdint>
#include <fstream>
#include <optional>
#include <string_view>
#include <type_traits>

#include "MLX90640_API.h"

class MLX90640 {
public:
    static constexpr auto WIDTH = 32u;
    static constexpr auto HEIGHT = 24u;
    using ToPtr = float (*) [WIDTH*HEIGHT];

    // Half of the pixels are refreshed at REFRESH_RATE
    enum class refresh_rate_t: uint8_t {
        _p5_HZ = 0u,
        _01_HZ = 1u,
        _02_HZ = 2u,
        _04_HZ = 3u,
        _08_HZ = 4u,
        _16_HZ = 5u,
        _32_HZ = 6u,
        _64_HZ = 7u
    };

    static constexpr auto RESOLUTION_16_BIT = 0u;
    static constexpr auto RESOLUTION_17_BIT = 1u;
    static constexpr auto RESOLUTION_18_BIT = 2u;
    static constexpr auto RESOLUTION_19_BIT = 3u;

    static constexpr auto INTERLEAVED_PATTERN_MODE = 0u;
    static constexpr auto CHESS_PATTERN_MODE = 1u;

    static constexpr auto MLX_I2C_ADDR = 0x33;
    static constexpr auto RESOLUTION = RESOLUTION_19_BIT;
    static constexpr auto PATTERN_MODE = CHESS_PATTERN_MODE;
    static constexpr auto EMISSIVITY = .95f;
    static constexpr auto TA_SHIFT = 8.f;

public:
    static bool dump_eeprom(std::string_view file_name) {
        std::fstream file { file_name.data(), file.binary | file.trunc | file.out };
        if( file.is_open() ){
            uint16_t EE[832];
            MLX90640_DumpEE(MLX_I2C_ADDR, EE);
            file.write(reinterpret_cast<char*>(EE), sizeof(EE));
            return true;
        }
        return false;
    }

    static std::optional<refresh_rate_t> request_refresh_rate(float desired) {
        constexpr auto eps = .1f;
        const auto desired_log1p = std::log2(desired) + 1.f;
        if( std::abs(desired_log1p - float(refresh_rate_t::_p5_HZ)) <= eps ){
            return refresh_rate_t::_p5_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_01_HZ)) <= eps ){
            return refresh_rate_t::_01_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_02_HZ)) <= eps ){
            return refresh_rate_t::_02_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_04_HZ)) <= eps ){
            return refresh_rate_t::_04_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_08_HZ)) <= eps ){
            return refresh_rate_t::_08_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_16_HZ)) <= eps ){
            return refresh_rate_t::_16_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_32_HZ)) <= eps ){
            return refresh_rate_t::_32_HZ;
        }else if( std::abs(desired_log1p - float(refresh_rate_t::_64_HZ)) <= eps ){
            return refresh_rate_t::_64_HZ;
        }else{
            return std::nullopt;
        }
    }

    explicit MLX90640(refresh_rate_t refresh_rate) {
        if( PATTERN_MODE == CHESS_PATTERN_MODE ){
            MLX90640_SetChessMode(MLX_I2C_ADDR);
        }else{
            MLX90640_SetInterleavedMode(MLX_I2C_ADDR);
        }
        MLX90640_SetRefreshRate(MLX_I2C_ADDR, static_cast<std::underlying_type_t<refresh_rate_t>>(refresh_rate));
        MLX90640_SetResolution(MLX_I2C_ADDR, RESOLUTION);
        uint16_t EE[832];
        MLX90640_DumpEE(MLX_I2C_ADDR, EE);
        MLX90640_ExtractParameters(EE, &mParams);
    };

    bool operator()(ToPtr To) {
        if( !MLX90640_GetFrame(MLX_I2C_ADDR, mFrame) ){
            return false;
        }
        const auto Ta = MLX90640_GetTa(mFrame, &mParams);
        MLX90640_CalculateTo(mFrame, &mParams, EMISSIVITY, Ta - TA_SHIFT, (float*)To);
        MLX90640_BadPixelsCorrection((&mParams)->brokenPixels, (float*)To, PATTERN_MODE, &mParams);
        MLX90640_BadPixelsCorrection((&mParams)->outlierPixels, (float*)To, PATTERN_MODE, &mParams);
        return true;
    }

private:
    uint16_t mFrame[834];
    paramsMLX90640 mParams;
};
