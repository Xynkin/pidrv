#pragma once

#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <optional>
#include <thread>
#include <tuple>

#include <spi.h>
#include <gpio.h>

template<
    unsigned _WIDTH,
    unsigned _HEIGHT,
    unsigned OFFSET_X,
    unsigned OFFSET_Y
>
class ST7789 {

public:
    static constexpr auto WIDTH = _WIDTH;
    static constexpr auto HEIGHT = _HEIGHT;

    struct __attribute__ ((packed)) RGB888 {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };
    static_assert(sizeof(RGB888) == 3u);

    struct RGBX8888 {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t x;
    };
    static_assert(sizeof(RGBX8888) == 4u);

    union RGB565 {
        using Packed = uint16_t;
        struct c {
            uint16_t r: 5;
            uint16_t g: 6;
            uint16_t b: 5;
        };
        Packed packed;


        RGB565(RGB888 rgb888)
            : packed{ uint16_t(((rgb888.r&0xF8)<<8)|((rgb888.g&0xFC)<<3)|(rgb888.b>>3)) }
        {}
        RGB565(RGBX8888 rgbx8888)
            : packed{ uint16_t(((rgbx8888.r&0xF8)<<8)|((rgbx8888.g&0xFC)<<3)|(rgbx8888.b>>3)) }
        {}
        RGB565(Packed packed)
            : packed{ packed }
        {}
    };
    static_assert(sizeof(RGB565) == sizeof(typename RGB565::Packed));

private:
    enum class cmd_t: uint8_t {
        NOP       = 0x00,
        SWRESET   = 0x01,
        RDDID     = 0x04,
        RDDST     = 0x09,

        SLPIN     = 0x10,
        SLPOUT    = 0x11,
        PTLON     = 0x12,
        NORON     = 0x13,

        INVOFF    = 0x20,
        INVON     = 0x21,
        DISPOFF   = 0x28,
        DISPON    = 0x29,

        CASET     = 0x2A,
        RASET     = 0x2B,
        RAMWR     = 0x2C,
        RAMRD     = 0x2E,

        PTLAR     = 0x30,
        TEOFF     = 0x34,
        TEON      = 0x35,
        MADCTL    = 0x36,
        COLMOD    = 0x3A,

        RAMCTRL   = 0xB0,
        FRMCTR1   = 0xB1,
        PORCTRL   = 0xB2,
        FRMCTR3   = 0xB3,
        INVCTR    = 0xB4,
        DISSET5   = 0xB6,

        GCTRL     = 0xB7,
        GTADJ     = 0xB8,
        VCOMS     = 0xBB,

        LCMCTRL   = 0xC0,
        IDSET     = 0xC1,
        VDVVRHEN  = 0xC2,
        VRHS      = 0xC3,
        VDVS      = 0xC4,
        VCMOFSET  = 0xC5,
        FRCTRL2   = 0xC6,
        CABCCTRL  = 0xC7,

        PVGAMCTRL = 0xE0,
        NVGAMCTRL = 0xE1
    };

    static constexpr uint8_t FPA_MIN  = 0x01;
    static constexpr uint8_t FPA_MAX  = 0x7F;
    static constexpr uint8_t BPA_MIN  = 0x01;
    static constexpr uint8_t BPA_MAX  = 0x7F;
    static constexpr uint8_t RTNA_MIN = 0x00;
    static constexpr uint8_t RTNA_MAX = 0x1F;

public:
    class refresh_controls_t {
    public:
        explicit refresh_controls_t()
            : FPA{ 0x0C }
            , BPA{ 0x0C }
            , RTNA{ 0x0F }
        {}
    private:
        explicit refresh_controls_t(uint8_t FPA, uint8_t BPA, uint8_t RTNA)
            : FPA{ FPA }
            , BPA{ BPA }
            , RTNA{ RTNA }
        {}
    private:
        uint8_t FPA, BPA, RTNA;
        friend ST7789;
    };
    static std::optional<refresh_controls_t> request_refresh_rate(float desired) {
        constexpr auto eps = .1f;
        uint8_t pa = 0x0C;
        for( ;; ){
            const auto max_refresh_rate = refresh_rate(pa, pa, RTNA_MAX);
            if( desired < max_refresh_rate && std::abs(desired - max_refresh_rate) > eps ){
                if( pa < FPA_MAX ) { ++pa; continue; } else return std::nullopt;
            }
            const auto min_refresh_rate = refresh_rate(pa, pa, RTNA_MIN);
            if( desired > min_refresh_rate && std::abs(desired - max_refresh_rate) > eps ){
                if( pa > FPA_MIN ) { --pa; continue; } else return std::nullopt;
            }
            break;
        }
        uint8_t l_rtna = RTNA_MIN, r_rtna = RTNA_MAX + 1u;
        while( r_rtna - l_rtna > 1 ){
            const auto m_rtna = uint8_t(l_rtna + (r_rtna - l_rtna) / 2u);
            if( desired > refresh_rate(pa, pa, m_rtna) ){
                r_rtna = m_rtna; continue;
            }else{
                l_rtna = m_rtna; continue;
            }
        }
        const auto ld_refresh_rate = std::abs(desired - refresh_rate(pa, pa, l_rtna));
        const auto rd_refresh_rate = std::abs(desired - refresh_rate(pa, pa, r_rtna));
        return refresh_controls_t{pa, pa, ld_refresh_rate > rd_refresh_rate ? r_rtna : l_rtna};
    }

    explicit ST7789(SPI& bus, GPIO::Pin dc, refresh_controls_t refresh_controls)
        : mBus{ bus }
        , mDC{ dc }
    {
        using namespace std::chrono_literals;

        cmd(cmd_t::SWRESET);
        std::this_thread::sleep_for(150ms);
        cmd(cmd_t::MADCTL, {0x70});
        cmd(cmd_t::COLMOD, {0x55});
        cmd(cmd_t::RAMCTRL, {0x00, 0xF8});
        cmd(cmd_t::GCTRL, {0x14});
        cmd(cmd_t::VCOMS, {0x37});
        cmd(cmd_t::VRHS, {0x12});
        cmd(cmd_t::PORCTRL, {refresh_controls.BPA, refresh_controls.FPA, 0x00, 0x33, 0x33});
        cmd(cmd_t::FRCTRL2, {refresh_controls.RTNA});
        // cmd(cmd_t::PVGAMCTRL,
        //     {0xD0, 0x04, 0x0D, 0x11, 0x13, 0x2B, 0x3F,
        //     0x54, 0x4C, 0x18, 0x0D, 0x0B, 0x1F, 0x23});
        // cmd(cmd_t::NVGAMCTRL,
        //     {0xD0, 0x04, 0x0C, 0x11, 0x13, 0x2C, 0x3F,
        //     0x44, 0x51, 0x2F, 0x1F, 0x1F, 0x20, 0x23});
        cmd(cmd_t::INVON);
        cmd(cmd_t::SLPOUT);
        // cmd(cmd_t::TEON, {0x00});
        cmd(cmd_t::DISPON);
        std::this_thread::sleep_for(100ms);
    }
    ST7789(const ST7789&) = delete;
    ST7789(ST7789&&) = delete;
    ST7789& operator=(ST7789) = delete;
    ST7789& operator=(const ST7789&) = delete;
    ST7789& operator=(ST7789&&) = delete;
    ~ST7789() = default;

    void operator()(RGB565 color) {
        window();
        std::array<typename RGB565::Packed, WIDTH*HEIGHT> packed_buf;
        std::ranges::fill(packed_buf, color.packed);
        mBus.send(
            (const uint8_t*)packed_buf.data(),
            packed_buf.size()*sizeof(typename decltype(packed_buf)::value_type));

    }
    void operator()(const RGB888 (&buf)[WIDTH*HEIGHT]) {
        window();
        std::array<typename RGB565::Packed, WIDTH*HEIGHT> packed_buf;
        const auto* p = buf;
        for( auto x = 0u; x < WIDTH; ++x ){
            for( auto y = 0u; y < HEIGHT; ++y ){
                packed_buf[((HEIGHT - 1u) - y)*WIDTH + x] = RGB565{*p++}.packed;
            }
        }
        mBus.send(
            (const uint8_t*)packed_buf.data(),
            packed_buf.size()*sizeof(typename decltype(packed_buf)::value_type));
    }
    void operator()(const RGBX8888 (&buf)[WIDTH*HEIGHT]) {
        window();
        std::array<typename RGB565::Packed, WIDTH*HEIGHT> packed_buf;
        const auto* p = buf;
        for( auto x = 0u; x < WIDTH; ++x ){
            for( auto y = 0u; y < HEIGHT; ++y ){
                packed_buf[((HEIGHT - 1u) - y)*WIDTH + x] = RGB565{*p++}.packed;
            }
        }
        mBus.send(
            (const uint8_t*)packed_buf.data(),
            packed_buf.size()*sizeof(typename decltype(packed_buf)::value_type));
    }

private:
    static float refresh_rate(uint8_t FPA, uint8_t BPA, uint8_t RTNA) {
        return 10e+6f/((320.f + FPA + BPA)*(245.f + RTNA*16u));
    }

    void window(
        unsigned x0 = 0u, unsigned y0 = 0u,
        unsigned x1 = WIDTH - 1u, unsigned y1 = HEIGHT - 1u
    ){
        x0 += OFFSET_X;
        x1 += OFFSET_X;
        y0 += OFFSET_Y;
        y1 += OFFSET_Y;
        cmd(cmd_t::CASET,
            {uint8_t(x0>>8), uint8_t(x0), uint8_t(x1>>8), uint8_t(x1)});
        cmd(cmd_t::RASET,
            {uint8_t(y0>>8), uint8_t(y0), uint8_t(y1>>8), uint8_t(y1)});
        cmd(cmd_t::RAMWR);
    }

    void cmd(cmd_t cmd, std::initializer_list<uint8_t> data = {}) const {
        mDC(false);
        mBus.send({uint8_t(cmd)});
        mDC(true);
        if( data.size() ){
            mBus.send(data);
        }
    }
private:
    SPI& mBus;
    GPIO::Pin mDC;
};
