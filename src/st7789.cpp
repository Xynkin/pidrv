#include <st7789.h>

struct Context {
    using Display = ST7789<240u, 240u, 40u, 0u>;

    SPI bus;
    GPIO pins;
    Display display;

    explicit Context(unsigned SPI_SPEED_HZ, std::optional<Display::refresh_controls_t> refresh_controls)
        : bus {
            SPI{ "/dev/spidev0.1" }
            .mode(SPI_MODE_0)
            .lsb_first(false)
            .bits_per_word(8)
            .speed(SPI_SPEED_HZ)
        }
        , pins {
            GPIO{ "gpiochip0" }
            .outputs({{9, false}, {19, true}})
        }
        , display {
            bus,
            pins[9],
            refresh_controls.value_or(Display::refresh_controls_t{})
        }
    {
        if( !refresh_controls ){
            puts("Unsupported ST7789_FPS value, default refresh rate will be used");
        }
    }
};

Context* ctx = nullptr;

void __attribute__ ((constructor)) st7789_init(void) {
    puts("st7789_init");
    if( !ctx ){
        ctx = new Context{
            secure_getenv("ST7789_SPI_SPEED_MHZ")
            ? unsigned(1_MHz*std::strtoul(secure_getenv("ST7789_SPI_SPEED_MHZ"), nullptr, 10))
            : unsigned(90_MHz),
            Context::Display::request_refresh_rate(
                secure_getenv("ST7789_FPS")
                ? std::strtof(secure_getenv("ST7789_FPS"), nullptr)
                : 60.f)};
    }
}

void __attribute__ ((destructor)) st7789_cleanup(void) {
    puts("st7789_cleanup");
    if( ctx ){
        delete ctx;
    }
}

extern "C"
void putRGB(uint8_t* buf) {
    if( buf ){
        ctx->display((const Context::Display::RGB888 (&)[Context::Display::WIDTH*Context::Display::HEIGHT])*buf);
    }else{
        puts("st7789_putRGB: NULL!");
    }
}

extern "C"
void putRGBX(uint8_t* buf) {
    if( buf ){
        ctx->display((const Context::Display::RGBX8888 (&)[Context::Display::WIDTH*Context::Display::HEIGHT])*buf);
    }else{
        puts("st7789_putRGBX: NULL!");
    }
}
