#include <cstdio>
#include <cstdlib>

#include <mlx90640.h>

struct Context {
    MLX90640 mlx90640;

    explicit Context(std::optional<MLX90640::refresh_rate_t> refresh_rate)
        : mlx90640 {refresh_rate.value_or(MLX90640::refresh_rate_t::_02_HZ)}
    {
        if( !refresh_rate ){
            puts("Unsupported MLX90640_FPS value, default refresh rate will be used");
        }
    }
};

Context* ctx = nullptr;

void __attribute__ ((constructor)) mlx90640_init(void) {
    puts("mlx90640_init");
    if( !ctx ){
        ctx = new Context{
            MLX90640::request_refresh_rate(
                secure_getenv("MLX90640_FPS") ? std::strtof(secure_getenv("MLX90640_FPS"), nullptr) : 2.f)};
    }
}

void __attribute__ ((destructor)) mlx90640_cleanup(void) {
    puts("mlx90640_cleanup");
    if( ctx ){
        delete ctx;
    }
}

extern "C"
bool get(float* buf) {
    return ctx->mlx90640((MLX90640::ToPtr)buf);
}
